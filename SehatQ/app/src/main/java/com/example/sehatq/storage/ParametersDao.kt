package com.example.sehatq.storage

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.sehatq.model.HistoryProduct

@Dao
interface ParametersDao {

    @Query("select * from HistoryProduct order by timestamp desc")
    suspend fun loadHistoryPurchase(): List<HistoryProduct>

    @Query("select * from HistoryProduct order by timestamp asc")
    suspend fun loadLastArticleRecord(): List<HistoryProduct>

    @Query("Delete from HistoryProduct")
    fun deleteHistoryProduct()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insertHistoryProduct(item: HistoryProduct?)

    @Query("update HistoryProduct set timestamp = :timestamp where title = :searchWord")
    fun updateHistoryProduct(searchWord: String, timestamp: String)

}