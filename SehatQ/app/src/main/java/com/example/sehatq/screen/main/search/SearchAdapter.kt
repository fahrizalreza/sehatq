package com.example.sehatq.screen.main.search

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.sehatq.R
import com.example.sehatq.model.HistoryProduct
import com.example.sehatq.model.ProductItem
import kotlinx.android.synthetic.main.item_product_list_layout.view.*

class SearchAdapter(val context: Context, var list: List<ProductItem>) :
    RecyclerView.Adapter<SearchAdapter.ViewHolder>(), Filterable {

    lateinit var listener: OnItemClickListener
    private var filteredList: List<ProductItem> = list

    // attach layout to item view
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product_list_layout, parent, false)
        return ViewHolder(v)
    }

    // submit size of list
    override fun getItemCount(): Int = list.size

    // perform action on recycler view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(context, list[position])
        val item = list[position]
        holder.itemView.setOnClickListener {
            listener.onClick(it, item)
        }
    }

    // set item content
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(context: Context, item: ProductItem?) {
            Log.e(LOG_PROCESS_ADAPTER, "item = ${item?.title}")

                Glide.with(context)
                    .load(item?.imageUrl)
                    .transition(DrawableTransitionOptions.withCrossFade(800))
                    .into(itemView.iv_product_list_layout_icon)

            itemView.tv_product_list_layout_product.text = item?.title
            itemView.tv_product_list_layout_price.text = item?.price
        }
    }

    // auto filter
    override fun getFilter(): Filter {
        return SearchFilter(this, filteredList)
    }

    // on click function
    interface OnItemClickListener {
        fun onClick(view: View, item: ProductItem?)
    }

    // click interface
    fun setItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    // constant value
    companion object {
        const val LOG_PROCESS_ADAPTER: String = "searchAdapter"
    }
}