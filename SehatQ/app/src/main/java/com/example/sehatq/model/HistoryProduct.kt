package com.example.sehatq.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "HistoryProduct")

data class HistoryProduct(

    @ColumnInfo(name = "roomId")
    @PrimaryKey(autoGenerate = true)
    val roomId: Long = 0,

    @ColumnInfo(name = "loved")
    @field:SerializedName("loved")
    val loved: Int? = null,

    @ColumnInfo(name = "price")
    @field:SerializedName("price")
    val price: String? = null,

    @ColumnInfo(name = "imageUrl")
    @field:SerializedName("imageUrl")
    val imageUrl: String? = null,

    @ColumnInfo(name = "description")
    @field:SerializedName("description")
    val description: String? = null,

    @ColumnInfo(name = "id")
    @field:SerializedName("id")
    val id: String? = null,

    @ColumnInfo(name = "title")
    @field:SerializedName("title")
    val title: String? = null,
    
    @ColumnInfo(name = "timestamp")
    var timestamp: String? = null
)