package com.example.sehatq.screen.main.ui.profile

import android.content.Context
import com.example.sehatq.model.HistoryProduct
import com.example.sehatq.storage.AppDatabase
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ProfileRepository(val context: Context) {

    lateinit var appDatabase: AppDatabase
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    // retrieve local database table history product
    suspend fun loadHistoryPurchase(): List<HistoryProduct> {
        appDatabase = AppDatabase.getInstance(context)
        return withContext(Dispatchers.IO) {

            appDatabase.parametersDao().loadHistoryPurchase()
        }
    }

    // perform when done
    fun onDestroy() {
        AppDatabase.destroyInstance()
        compositeDisposable.dispose()
    }
}