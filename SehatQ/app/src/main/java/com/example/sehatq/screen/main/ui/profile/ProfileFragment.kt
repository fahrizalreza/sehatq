package com.example.sehatq.screen.main.ui.profile

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sehatq.R
import com.example.sehatq.base.BaseFragment
import com.example.sehatq.model.HistoryProduct
import com.example.sehatq.screen.main.detailproduct.DetailProductActivity
import com.example.sehatq.screen.main.ui.main.MainFragment
import kotlinx.android.synthetic.main.fragment_profile.view.*


class ProfileFragment : BaseFragment(), View.OnClickListener {

    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var rvHistoryPurchase: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        profileViewModel = ViewModelProvider(
            requireActivity(),
            viewModelFactory { ProfileViewModel(context?.applicationContext!!) }).get(
            ProfileViewModel::class.java
        )
        val fragmentView = inflater.inflate(R.layout.fragment_profile, container, false)
        (activity as AppCompatActivity).supportActionBar?.hide()

        rvHistoryPurchase = fragmentView.findViewById(R.id.rv_profile_list) as RecyclerView
        fragmentView.iv_profile_list_arrow_back.setOnClickListener(this)
//        actionBar?.hide()
//        val loadingBar: ProgressBar = fragmentView.findViewById(R.id.text_profile)
        profileViewModel.retrieveIsLoading().observe(viewLifecycleOwner, Observer {
            //            textView.text = it
        })
        profileViewModel.retrieveListData().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                setAdapter(it)
            }
        })

        profileViewModel.loadContentLocal()

        return fragmentView
    }

    // perform when user click
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_profile_list_arrow_back -> back()
        }
    }

    // connect data to recycler view via adapter
    private fun setAdapter(list: List<HistoryProduct?>?) {

        val adapter = ProfileAdapter(context?.applicationContext!!, list)
        rvHistoryPurchase.adapter = adapter
        rvHistoryPurchase.layoutManager =
            LinearLayoutManager(context?.applicationContext, RecyclerView.VERTICAL, false)
        rvHistoryPurchase.adapter?.notifyDataSetChanged()

        adapter.setItemClickListener(object :
            ProfileAdapter.OnItemClickListener {
            override fun onClick(view: View, item: HistoryProduct?) {
                openDetailProduct(item)
            }
        })

    }

    // open detail product activity
    private fun openDetailProduct(item: HistoryProduct?) {
        val intent = Intent(context?.applicationContext, DetailProductActivity::class.java)
        intent.putExtra(KEY_PRODUCT_ID, item?.id)
        intent.putExtra(KEY_PRODUCT_TITLE, item?.title)
        intent.putExtra(KEY_PRODUCT_LOVED, item?.loved)
        intent.putExtra(KEY_PRODUCT_PICTURE_URL, item?.imageUrl)
        intent.putExtra(KEY_PRODUCT_PRICE, item?.price)
        intent.putExtra(KEY_PRODUCT_DESCRIPTION, item?.description)
        Log.e(
            LOG_PROFILE_MENU,
            "open detail product = ${item?.id}, ${item?.title}, ${item?.loved}, ${item?.price}, ${item?.imageUrl}, ${item?.price}, ${item?.description}"
        )
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    // back to previous
    private fun back() {
        val fragment = MainFragment()
        val frgManager = fragmentManager
        val ft = frgManager!!.beginTransaction()
        ft.addToBackStack(null)
        ft.add(R.id.nav_host_fragment, fragment)
        ft.commit()
    }
    // constant value
    companion object {
        const val KEY_PRODUCT_ID: String = "productId"
        const val KEY_PRODUCT_TITLE: String = "productTitle"
        const val KEY_PRODUCT_PICTURE_URL: String = "productURL"
        const val KEY_PRODUCT_DESCRIPTION: String = "productDescription"
        const val KEY_PRODUCT_PRICE: String = "productPrice"
        const val KEY_PRODUCT_LOVED: String = "productLoved"
        const val LOG_PROFILE_MENU: String = "profileMenu"
    }
}