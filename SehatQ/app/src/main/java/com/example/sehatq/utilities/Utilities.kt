package com.example.sehatq.utilities

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.view.Window
import android.view.WindowManager
import java.text.SimpleDateFormat
import java.util.*

object Utilities {

    // check internet connection
    fun isInternetConnected(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val nw = connectivityManager.activeNetwork ?: return false
        val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
        return when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                true
            }
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                true
            }
            else -> false
        }
    }

    // re enable user action after loading bar disappear
    fun enableScreenAction(window: Window) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    // user can not do any action while loading bar appear
    fun disableScreenAction(window: Window) {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    // retrive current time
    fun retrieveCurrentTime(): String {
        val sdf = SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH)
        val currentDate = sdf.format(Date())

        return currentDate
    }
}