package com.example.sehatq.base

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sehatq.utilities.Utilities
import com.example.sehatq.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    private var job = Job()
    val coroutineScope = CoroutineScope(Dispatchers.Main + job)


    // for view model with parameter
    fun <VM : ViewModel> viewModelFactory(f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }

    // perform when user press back button
    override fun onBackPressed() {
        if (this.localClassName == "screen.main.MainActivity") {
            openDialog()
        }else {
            closePage()
        }
    }

    // check internet connection
    suspend fun isInternetConnected(context: Context): Boolean =
        withContext(Dispatchers.Default) {
            Utilities.isInternetConnected(context)
        }

    // confirmation box
    private fun openDialog() {
        val builder1 = AlertDialog.Builder(this)
        builder1.setMessage(resources.getString(R.string.exit_app_ind))
        builder1.setCancelable(true)
        builder1.setPositiveButton(
            resources.getString(R.string.yes_ind)
        ) { _, _ -> finish() }
        builder1.setNegativeButton(
            resources.getString(R.string.no_ind)
        ) { dialog, _ -> dialog.cancel() }
        val alert = builder1.create()
        alert.setOnShowListener {
            alert.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.black, null))
            alert.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(R.color.black, null))
        }
        alert.show()
    }

    // close with slide animation
    fun closePage() {
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    // pop message
    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}