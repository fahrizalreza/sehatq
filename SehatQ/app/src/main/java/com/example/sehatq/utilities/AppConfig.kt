package com.example.sehatq.utilities

object AppConfig {
    const val URL = "https://private-4639ce-ecommerce56.apiary-mock.com/"

    //Database Sqlite
    const val DATABASE_NAME = "revampdb.db"
    const val DATABASE_VERSION = 1
}