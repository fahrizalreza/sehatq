package com.example.sehatq.screen.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.sehatq.R
import com.example.sehatq.base.BaseActivity
import com.example.sehatq.screen.main.MainActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.firebase.ui.auth.AuthUI
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*


class LoginActivity : BaseActivity(), View.OnClickListener {

    private lateinit var providers: List<AuthUI.IdpConfig>
    private lateinit var callbackManager: CallbackManager
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
    }

    // after filled form
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        openMainMenu()
    }

    // action on user click
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_login_sign_in -> openMainMenu()
            R.id.btn_google -> signIn()
        }
    }

    // perform on first open
    private fun init() {

        btn_login_sign_in.setOnClickListener(this)
        btn_google.setOnClickListener(this)

        setAndroid()
        setFacebook()

//        showSignInOption()
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_CODE);
    }

    // set android
    private fun setAndroid() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        val account = GoogleSignIn.getLastSignedInAccount(this)
    }

    private fun setFacebook() {
        btn_facebook.setReadPermissions(listOf("email", "public_profile"))
        callbackManager = CallbackManager.Factory.create()
        btn_facebook.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(p0: LoginResult?) {
                openMainMenu()
            }

            override fun onCancel() {

            }

            override fun onError(p0: FacebookException?) {

            }

        })
    }

    // open main menu
    private fun openMainMenu() {
        val intent = Intent(this, MainActivity::class.java)

        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        finish()
    }

    // alternatives
    fun showSignInOption() {
        providers = Arrays.asList<AuthUI.IdpConfig>(
            AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.PhoneBuilder().build()
        )
        startActivityForResult(
            AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setTheme(R.style.MyTheme)
                .build(), REQUEST_CODE
        )
    }


    // constant value
    companion object {
        const val REQUEST_CODE: Int = 7117
        const val LOG_INTRO: String = "intro"
    }
}
