package com.example.sehatq.screen.main.detailproduct

import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.sehatq.model.HistoryProduct
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class DetailProductViewModel(val context: Context) : ViewModel(), CoroutineScope {

    private var repository = DetailProductRepository(context)
    private var job = Job()

    // set co routine
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    // add to history purchase
    fun transaction(item: HistoryProduct) {
        launch { repository.insertTableContent(item) }
    }
}