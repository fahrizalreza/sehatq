package com.example.sehatq.screen.main.ui.main

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.sehatq.R
import com.example.sehatq.model.CategoryItem
import com.example.sehatq.model.ProductItem
import kotlinx.android.synthetic.main.item_category_list_layout.view.*
import kotlinx.android.synthetic.main.item_product_sale_list_layout.view.*

class MainProductSaleAdapter(val context: Context, private var list: List<ProductItem?>?) :
    RecyclerView.Adapter<MainProductSaleAdapter.ViewHolder>() {

    lateinit var listener: OnItemClickListener

    // attach layout to item view
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product_sale_list_layout, parent, false)
        return ViewHolder(v)
    }

    // submit size of list
    override fun getItemCount(): Int = list!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(context, list?.get(position))
        val item = list?.get(position)
        holder.itemView.setOnClickListener {
            listener.onClick(it, item)
        }
    }

    // set item content
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(context: Context, item: ProductItem?) {
            Log.e(LOG_PROCESS_ADAPTER, "item = ${item?.title}")
            Glide.with(context)
                .load(item?.imageUrl)
                .transition(DrawableTransitionOptions.withCrossFade(800))
                .error(R.drawable.ic_document)
                .into(itemView.iv_product_sale_list_layout_rectangle)

            if (item?.loved == 0) {
                Glide.with(context)
                    .load(R.drawable.ic_favorite_black_24dp)
                    .transition(DrawableTransitionOptions.withCrossFade(800))
                    .into(itemView.iv_product_sale_list_layout_fav)
            } else {
                Glide.with(context)
                    .load(R.drawable.ic_favorite_red_24dp)
                    .transition(DrawableTransitionOptions.withCrossFade(800))
                    .into(itemView.iv_product_sale_list_layout_fav)
            }

            itemView.tv_product_sale_list_layout_title.text = item?.title
        }
    }

    // on click function
    interface OnItemClickListener {
        fun onClick(view: View, item: ProductItem?)
    }

    // click interface
    fun setItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    // constant value
    companion object {
        const val LOG_PROCESS_ADAPTER: String = "MainProductSaleAdapter"
    }
}