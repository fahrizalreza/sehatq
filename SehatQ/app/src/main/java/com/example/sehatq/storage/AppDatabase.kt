package com.example.sehatq.storage

import android.content.Context
import androidx.room.*
import com.example.sehatq.model.HistoryProduct
import com.example.sehatq.utilities.AppConfig
import com.example.sehatq.utilities.ParentConverter

@Database(
    entities = [HistoryProduct::class], version = AppConfig.DATABASE_VERSION
)
@TypeConverters(ParentConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun parametersDao(): ParametersDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java, AppConfig.DATABASE_NAME
                    )
                        .build()
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}