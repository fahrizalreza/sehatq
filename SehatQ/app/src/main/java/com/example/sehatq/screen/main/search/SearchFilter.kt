package com.example.sehatq.screen.main.search

import android.widget.Filter
import com.example.sehatq.model.ProductItem
import java.util.*
import kotlin.collections.ArrayList

class SearchFilter(var adapter: SearchAdapter, val list: List<ProductItem>) : Filter() {

    // perform on user type character
    override fun performFiltering(constraint: CharSequence?): FilterResults {
        val filterResult = FilterResults()
        val filteredItem = ArrayList<ProductItem>()

        if (!constraint.isNullOrBlank() && constraint.isNotEmpty()) {
            val charString = constraint.toString().toLowerCase(Locale.UK)

            for (item in list) {

                if (item.title.toString().toLowerCase(Locale.UK).contains(charString)) {
                    filteredItem.add(item)
                }
            }
            filterResult.count = filteredItem.size
            filterResult.values = filteredItem
        } else {
            filterResult.count = list.size
            filterResult.values = list
        }
        return filterResult
    }

    // publish filter result
    override fun publishResults(p0: CharSequence?, results: FilterResults?) {
        adapter.list = results?.values as ArrayList<ProductItem>
        adapter.notifyDataSetChanged()
    }
}