package com.example.sehatq.screen.main.detailproduct

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.sehatq.R
import com.example.sehatq.base.BaseActivity
import com.example.sehatq.model.HistoryProduct
import com.example.sehatq.utilities.Utilities
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.activity_detail_product.*
import kotlinx.coroutines.launch

class DetailProductActivity : BaseActivity(), View.OnClickListener {

    private var id = ""
    private var title = ""
    private var price = ""
    private var loved = 0
    private var url = ""
    private var description = ""
    private var isHide: Boolean = true
    lateinit var viewModel: DetailProductViewModel

    // perform on first run
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_product)

        init()
    }

    // perform action when user click item
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_detail_product_arrow_back -> closePage()
            R.id.tv_detail_product_buy -> settlement()
        }
    }

    // initialize first process
    private fun init() {
        id = intent.getStringExtra(KEY_PRODUCT_ID) ?: ""
        title = intent.getStringExtra(KEY_PRODUCT_TITLE) ?: ""
        price = intent.getStringExtra(KEY_PRODUCT_PRICE) ?: ""
        loved = intent.getIntExtra(KEY_PRODUCT_LOVED, 0)
        url = intent.getStringExtra(KEY_PRODUCT_PICTURE_URL) ?: ""
        description = intent.getStringExtra(KEY_PRODUCT_DESCRIPTION) ?: ""

        iv_detail_product_arrow_back.setOnClickListener(this)
        tv_detail_product_buy.setOnClickListener(this)

        Glide.with(this)
            .load(url)
            .transition(DrawableTransitionOptions.withCrossFade(500))
            .error(R.drawable.iv_example)
            .into(iv_detail_product_rectangle)
        tv_detail_product_item_name.text = title
        if (loved == 0) {
            Glide.with(this)
                .load(R.drawable.ic_favorite_black_24dp)
                .transition(DrawableTransitionOptions.withCrossFade(500))
                .error(R.drawable.iv_example)
                .into(iv_detail_product_favourite)
        } else {
            Glide.with(this)
                .load(R.drawable.ic_favorite_red_24dp)
                .transition(DrawableTransitionOptions.withCrossFade(500))
                .error(R.drawable.iv_example)
                .into(iv_detail_product_favourite)
        }
        tv_detail_product_id.text = id
        tv_detail_product_description.text = description
        tv_detail_product_price.text = price

        setScrollAppBar()

        viewModel = ViewModelProvider(
            this,
            viewModelFactory { DetailProductViewModel(this) }).get(
            DetailProductViewModel::class.java
        )
    }

    // transaction settlement
    private fun settlement() {
        coroutineScope.launch {
            val item = HistoryProduct(0, loved, price, url, description, id, title, Utilities.retrieveCurrentTime())
            viewModel.transaction(item)}
        showToast("Payment already submitted")
    }

    // perform when user scroll up or scroll down
    private fun setScrollAppBar() {

        // set title
        abl_detail_product_title.addOnOffsetChangedListener(object :
            AppBarLayout.OnOffsetChangedListener {
            var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }

                //When Scroll up, picture hide
                if (scrollRange + verticalOffset == 0) {
                    tv_detail_product_back.text = title
                    toolbar_detail_product.setBackgroundColor(
                        resources.getColor(
                            R.color.blue,
                            null
                        )
                    )
                    isHide = true

                } else if (isHide) {

                    // when picture appear
                    toolbar_detail_product.setBackgroundColor(Color.TRANSPARENT)
                    tv_detail_product_back.text = " "
                    //careful there should a space between double quote otherwise it wont work
                    isHide = false
                }
            }
        })

    }

    // constant value
    companion object {
        const val KEY_PRODUCT_ID: String = "productId"
        const val KEY_PRODUCT_TITLE: String = "productTitle"
        const val KEY_PRODUCT_PICTURE_URL: String = "productURL"
        const val KEY_PRODUCT_DESCRIPTION: String = "productDescription"
        const val KEY_PRODUCT_PRICE: String = "productPrice"
        const val KEY_PRODUCT_LOVED: String = "productLoved"
        const val LOG_DETAIL_PRODUCT: String = "detailProduct"
    }
}
