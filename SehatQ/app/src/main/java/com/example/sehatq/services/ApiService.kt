package com.example.sehatq.services

import com.example.sehatq.model.ProductData
import com.example.sehatq.model.ProductResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    // all category
    @GET("home")
    fun loadProduct(): Single<ProductResponse>
}