package com.example.sehatq.screen.main.search

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sehatq.R
import com.example.sehatq.base.BaseActivity
import com.example.sehatq.model.ProductItem
import com.example.sehatq.screen.main.detailproduct.DetailProductActivity
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : BaseActivity(), View.OnClickListener {

    private lateinit var adapter: SearchAdapter
    lateinit var viewModel: SearchViewModel

    // perform on first open
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        init()
    }

    // perform when user click
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_search_list_arrow_back -> closePage()
        }
    }

    // initialize first process
    private fun init() {
        iv_search_list_arrow_back.setOnClickListener(this)

        rv_search_list.visibility = View.GONE
        loadData()
        et_search_bar_product.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                rv_search_list.visibility = View.VISIBLE
                adapter.filter.filter(s)
                adapter.notifyDataSetChanged()
            }

        })

    }

    // load data from local
    private fun loadData() {
        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
            .create(SearchViewModel::class.java)
        viewModel.retrieveList()
            .observe(this, Observer {
                if (it != null) {
                    setAdapter(it)
                }
            })
        viewModel.loadData()
    }


    // connect data to recycler view via adapter
    private fun setAdapter(list: List<ProductItem>) {

        adapter = SearchAdapter(this, list)
        rv_search_list.adapter = adapter
        rv_search_list.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        adapter.setItemClickListener(object :
            SearchAdapter.OnItemClickListener {
            override fun onClick(view: View, item: ProductItem?) {
                openDetailProduct(item)
            }
        })

    }

    // open detail product activity
    private fun openDetailProduct(item: ProductItem?) {
        val intent = Intent(this, DetailProductActivity::class.java)
        intent.putExtra(KEY_PRODUCT_ID, item?.id)
        intent.putExtra(KEY_PRODUCT_TITLE, item?.title)
        intent.putExtra(KEY_PRODUCT_LOVED, item?.loved)
        intent.putExtra(KEY_PRODUCT_PICTURE_URL, item?.imageUrl)
        intent.putExtra(KEY_PRODUCT_PRICE, item?.price)
        intent.putExtra(KEY_PRODUCT_DESCRIPTION, item?.description)
        Log.e(
            LOG_SEARCH_FEATURE,
            "open detail product = ${item?.id}, ${item?.title}, ${item?.loved}, ${item?.price}, ${item?.imageUrl}, ${item?.price}, ${item?.description}"
        )
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    // constant value
    companion object {
        const val KEY_PRODUCT_ID: String = "productId"
        const val KEY_PRODUCT_TITLE: String = "productTitle"
        const val KEY_PRODUCT_PICTURE_URL: String = "productURL"
        const val KEY_PRODUCT_DESCRIPTION: String = "productDescription"
        const val KEY_PRODUCT_PRICE: String = "productPrice"
        const val KEY_PRODUCT_LOVED: String = "productLoved"
        const val LOG_SEARCH_FEATURE: String = "searchFeature"

    }
}