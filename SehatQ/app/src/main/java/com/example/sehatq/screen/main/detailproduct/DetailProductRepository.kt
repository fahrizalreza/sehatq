package com.example.sehatq.screen.main.detailproduct

import android.content.Context
import com.example.sehatq.model.HistoryProduct
import com.example.sehatq.storage.AppDatabase
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DetailProductRepository(val context: Context) {

    lateinit var appDatabase: AppDatabase
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    // insert to database local
    suspend fun insertTableContent(item: HistoryProduct) {
        appDatabase = AppDatabase.getInstance(context)
        return withContext(Dispatchers.IO) {

            appDatabase.parametersDao().insertHistoryProduct(item)
        }
    }
}