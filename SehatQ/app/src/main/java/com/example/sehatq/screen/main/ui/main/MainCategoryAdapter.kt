package com.example.sehatq.screen.main.ui.main

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.sehatq.R
import com.example.sehatq.model.CategoryItem
import kotlinx.android.synthetic.main.item_category_list_layout.view.*

class MainCategoryAdapter(val context: Context, private var list: List<CategoryItem?>?) :
    RecyclerView.Adapter<MainCategoryAdapter.ViewHolder>() {

    lateinit var listener: OnItemClickListener

    // attach layout to item view
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_category_list_layout, parent, false)
        return ViewHolder(v)
    }

    // submit size of list
    override fun getItemCount(): Int = list!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(context, list?.get(position))
        val item = list?.get(position)
        holder.itemView.setOnClickListener {
            listener.onClick(it, item)
        }
    }

    // set item content
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(context: Context, item: CategoryItem?) {
            Log.e(LOG_PROCESS_ADAPTER, "item = ${item?.name}")
            Glide.with(context)
                .load(item?.imageUrl)
                .transition(DrawableTransitionOptions.withCrossFade(800))
                .error(R.drawable.ic_document)
                .into(itemView.iv_main_category_icon)

            itemView.tv_main_category_category.text = item?.name
        }
    }

    // on click function
    interface OnItemClickListener {
        fun onClick(view: View, item: CategoryItem?)
    }

    // click interface
    fun setItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    // constant value
    companion object {
        const val LOG_PROCESS_ADAPTER: String = "MainCategoryAdapter"
    }
}