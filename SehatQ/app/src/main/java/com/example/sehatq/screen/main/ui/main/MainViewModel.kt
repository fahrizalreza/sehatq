package com.example.sehatq.screen.main.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sehatq.model.CategoryItem
import com.example.sehatq.model.ProductItem
import com.example.sehatq.network.ApiClientRx
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel : ViewModel() {
    val listCategoryDummy = mutableListOf<CategoryItem>(
        CategoryItem("https://img.icons8.com/bubbles/2x/t-shirt.png", "Baju", 21),
        CategoryItem("https://img.icons8.com/flat_round/2x/long-shorts.png", "Celana", 42),
        CategoryItem("https://img.icons8.com/flat_round/2x/summer-hat.png", "Topi", 1),
        CategoryItem("https://img.icons8.com/color/2x/red-purse.png", "Tas", 91),
        CategoryItem("https://img.icons8.com/cute-clipart/2x/apple-watch-apps.png", "Jam Tangan", 131)
    )
    val listProductSaleDummy = mutableListOf<ProductItem>(
        ProductItem(0, "$340", "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Nintendo-Switch-Console-Docked-wJoyConRB.jpg/430px-Nintendo-Switch-Console-Docked-wJoyConRB.jpg", "The Nintendo Switch was released on March 3, 2017 and is Nintendo's second entry in the eighth generation of home video game consoles. The system was code-named Nintendo NX prior to its official announcement. It is a hybrid device that can be used as a home console inserted to the Nintendo Switch Dock attached to a television, stood up on a table with the kickstand, or as a tablet-like portable console. It features two detachable wireless controllers called Joy-Con, that can be used individually or attached to a grip to provide a more traditional game pad form. Both Joy-Con are built with motion sensors and HD Rumble, Nintendo's haptic vibration feedback system for improved gameplay experiences. However, only the right Joy-Con has an NFC reader on its analog joystick for Amiibo and an IR sensor on the back. The Nintendo Switch Pro Controller is a traditional style controller much like the one of the Gamecube.", "6723", "Nitendo Switch" ),
        ProductItem(1, "$70", "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/NES-Console-Set.jpg/430px-NES-Console-Set.jpg", "Released July 15, 1983, the Nintendo Entertainment System (NES) is an 8-bit video game console released by Nintendo in North America, South America, Europe, Asia, Oceania and Africa and was Nintendo's first home video game console released outside Japan. In Japan, it is known as the Family Computer (or Famicom, as it is commonly abbreviated). Selling 61.91 million units worldwide, the NES helped revitalize the video game industry following the video game crash of 1983 and set the standard for subsequent consoles in everything from game design to business practices. The NES was the first console for which the manufacturer openly courted third-party developers. Many of Nintendo's most iconic franchises, such as The Legend of Zelda and Metroid were started on the NES. Nintendo continued to repair Famicom consoles in Japan until October 31, 2007, attributing the decision to discontinue support to an increasing shortage of the necessary parts.[4][5][6]Nintendo released a software-emulation-based version of the Nintendo Entertainment System on November 10, 2016. Called the NES Classic Edition, it is a dedicated console that comes with a single controller and 30 preloaded games.[7]", "6724", "Nitendo Entertainment System "),
        ProductItem(1, "$110", "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Nintendo-ds-lite.svg/430px-Nintendo-ds-lite.svg.png", "The Nintendo DS (abbreviated NDS, DS, or the full name Nintendo Dual Screen, and iQue DS in China) is a handheld game console developed and manufactured by Nintendo, released in 2004. It is visibly distinguishable by its horizontal clamshell design, and the presence of two displays, the lower of which acts as a touchscreen. The system also has a built-in microphone and supports wireless IEEE 802.11 (Wi-Fi) standards, allowing players to interact with each other within short range (10–30 meters, depending on conditions) or over the Nintendo Wi-Fi Connection service via a standard Wi-Fi access point. According to Nintendo, the letters DS in the name stand for Developers' System and Double Screen, the former of which refers to the features of the handheld designed to encourage innovative gameplay ideas among developers.[43] The system was known as Project Nitro during development.", "6725", "Nitendo DS Lite "),
        ProductItem(0, "$200", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Wii-console.jpg/430px-Wii-console.jpg", "The Nintendo DS (abbreviated NDS, DS, or the full name Nintendo Dual Screen, and iQue DS in China) is a handheld game console developed and manufactured by Nintendo, released in 2004. It is visibly distinguishable by its horizontal clamshell design, and the presence of two displays, the lower of which acts as a touchscreen. The system also has a built-in microphone and supports wireless IEEE 802.11 (Wi-Fi) standards, allowing players to interact with each other within short range (10–30 meters, depending on conditions) or over the Nintendo Wi-Fi Connection service via a standard Wi-Fi access point. According to Nintendo, the letters DS in the name stand for Developers' System and Double Screen, the former of which refers to the features of the handheld designed to encourage innovative gameplay ideas among developers.[43] The system was known as Project Nitro during development.", "6726", "Nitendo WII " )
    )
    var listCategory: MutableLiveData<List<CategoryItem?>?> = MutableLiveData(listCategoryDummy)
    var listProductSale: MutableLiveData<List<ProductItem?>?> = MutableLiveData(listProductSaleDummy)
    var isLoading = MutableLiveData<Boolean>()

    // is connection still update
    fun retrieveIsLoading(): MutableLiveData<Boolean> = isLoading

    // retrieve value of Category
    fun retrieveListCategory(): MutableLiveData<List<CategoryItem?>?> = listCategory

    // retrieve value of product sale
    fun retrieveListProductSale(): MutableLiveData<List<ProductItem?>?> = listProductSale

    // load data from internet
    fun loadData() {
        isLoading.value = true
        val compositeDisposable = CompositeDisposable()
        val call = ApiClientRx().apiService.loadProduct()
        val disposable = call.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                Log.e(LOG_MAIN_CONNECTION, "connect to api master data = \n$result")
                listCategory.postValue(result.data?.category)
                listProductSale.postValue(result.data?.productPromo)
                isLoading.postValue(false)

            }, { error ->
                Log.e(LOG_MAIN_CONNECTION, "error connection: ${error.message} ")
                listCategory.postValue(null)
                listProductSale.postValue(null)

            })
        compositeDisposable.add(disposable)

    }

    // set value of Category
    fun setListCategory(list: List<CategoryItem>) {
        listCategory = MutableLiveData(list)
    }

    // set value of Category
    fun setListProductSale(list: List<ProductItem>) {
        listProductSale = MutableLiveData(list)
    }

    // constant value
    companion object {
        const val ERROR_SYSTEM: String =
            "Terjadi kesalahan pada sistem, silahkan hubungi customer service kami"
        const val LOG_MAIN_CONNECTION: String = "SourceConnection"
        const val PROCESS_LOAD_DATA: String = "processLoadData"
    }
}