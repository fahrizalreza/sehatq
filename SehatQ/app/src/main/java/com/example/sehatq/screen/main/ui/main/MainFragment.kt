package com.example.sehatq.screen.main.ui.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sehatq.R
import com.example.sehatq.base.BaseFragment
import com.example.sehatq.model.CategoryItem
import com.example.sehatq.model.ProductItem
import com.example.sehatq.screen.main.detailproduct.DetailProductActivity
import com.example.sehatq.screen.main.search.SearchActivity
import com.example.sehatq.utilities.Utilities

class MainFragment : BaseFragment(), View.OnClickListener {

    private lateinit var mainViewModel: MainViewModel
    val listCategory = mutableListOf<CategoryItem>(
        CategoryItem("https://img.icons8.com/bubbles/2x/t-shirt.png", "Baju", 21),
        CategoryItem("https://img.icons8.com/flat_round/2x/long-shorts.png", "Celana", 42),
        CategoryItem("https://img.icons8.com/flat_round/2x/summer-hat.png", "Topi", 1),
        CategoryItem("https://img.icons8.com/color/2x/red-purse.png", "Tas", 91),
        CategoryItem(
            "https://img.icons8.com/cute-clipart/2x/apple-watch-apps.png",
            "Jam Tangan",
            131
        )
    )
    val listProductSale = mutableListOf<ProductItem>(
        ProductItem(
            0,
            "$340",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Nintendo-Switch-Console-Docked-wJoyConRB.jpg/430px-Nintendo-Switch-Console-Docked-wJoyConRB.jpg",
            "The Nintendo Switch was released on March 3, 2017 and is Nintendo's second entry in the eighth generation of home video game consoles. The system was code-named Nintendo NX prior to its official announcement. It is a hybrid device that can be used as a home console inserted to the Nintendo Switch Dock attached to a television, stood up on a table with the kickstand, or as a tablet-like portable console. It features two detachable wireless controllers called Joy-Con, that can be used individually or attached to a grip to provide a more traditional game pad form. Both Joy-Con are built with motion sensors and HD Rumble, Nintendo's haptic vibration feedback system for improved gameplay experiences. However, only the right Joy-Con has an NFC reader on its analog joystick for Amiibo and an IR sensor on the back. The Nintendo Switch Pro Controller is a traditional style controller much like the one of the Gamecube.",
            "6723",
            "Nitendo Switch"
        ),
        ProductItem(
            1,
            "$70",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/NES-Console-Set.jpg/430px-NES-Console-Set.jpg",
            "Released July 15, 1983, the Nintendo Entertainment System (NES) is an 8-bit video game console released by Nintendo in North America, South America, Europe, Asia, Oceania and Africa and was Nintendo's first home video game console released outside Japan. In Japan, it is known as the Family Computer (or Famicom, as it is commonly abbreviated). Selling 61.91 million units worldwide, the NES helped revitalize the video game industry following the video game crash of 1983 and set the standard for subsequent consoles in everything from game design to business practices. The NES was the first console for which the manufacturer openly courted third-party developers. Many of Nintendo's most iconic franchises, such as The Legend of Zelda and Metroid were started on the NES. Nintendo continued to repair Famicom consoles in Japan until October 31, 2007, attributing the decision to discontinue support to an increasing shortage of the necessary parts.[4][5][6]Nintendo released a software-emulation-based version of the Nintendo Entertainment System on November 10, 2016. Called the NES Classic Edition, it is a dedicated console that comes with a single controller and 30 preloaded games.[7]",
            "6724",
            "Nitendo Entertainment System "
        ),
        ProductItem(
            1,
            "$110",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Nintendo-ds-lite.svg/430px-Nintendo-ds-lite.svg.png",
            "The Nintendo DS (abbreviated NDS, DS, or the full name Nintendo Dual Screen, and iQue DS in China) is a handheld game console developed and manufactured by Nintendo, released in 2004. It is visibly distinguishable by its horizontal clamshell design, and the presence of two displays, the lower of which acts as a touchscreen. The system also has a built-in microphone and supports wireless IEEE 802.11 (Wi-Fi) standards, allowing players to interact with each other within short range (10–30 meters, depending on conditions) or over the Nintendo Wi-Fi Connection service via a standard Wi-Fi access point. According to Nintendo, the letters DS in the name stand for Developers' System and Double Screen, the former of which refers to the features of the handheld designed to encourage innovative gameplay ideas among developers.[43] The system was known as Project Nitro during development.",
            "6725",
            "Nitendo DS Lite "
        ),
        ProductItem(
            0,
            "$200",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Wii-console.jpg/430px-Wii-console.jpg",
            "The Nintendo DS (abbreviated NDS, DS, or the full name Nintendo Dual Screen, and iQue DS in China) is a handheld game console developed and manufactured by Nintendo, released in 2004. It is visibly distinguishable by its horizontal clamshell design, and the presence of two displays, the lower of which acts as a touchscreen. The system also has a built-in microphone and supports wireless IEEE 802.11 (Wi-Fi) standards, allowing players to interact with each other within short range (10–30 meters, depending on conditions) or over the Nintendo Wi-Fi Connection service via a standard Wi-Fi access point. According to Nintendo, the letters DS in the name stand for Developers' System and Double Screen, the former of which refers to the features of the handheld designed to encourage innovative gameplay ideas among developers.[43] The system was known as Project Nitro during development.",
            "6726",
            "Nitendo WII "
        )
    )

    private lateinit var etSearch: TextView
    private lateinit var rvCategory: RecyclerView
    private lateinit var rvProductSale: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainViewModel =
            ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
        val fragmentView = inflater.inflate(R.layout.fragment_home, container, false)

//         loadingProgressBar = fragmentView.findViewById(R.id.ll_progress_main_menu) as LinearLayout
        etSearch = fragmentView.findViewById(R.id.et_main_search_product)
        rvCategory = fragmentView.findViewById(R.id.rv_main_category_list) as RecyclerView
        rvProductSale = fragmentView.findViewById(R.id.rv_main_product_sale_list) as RecyclerView


        etSearch.setOnClickListener(this)

        mainViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                showLoading()
            } else {
                hideLoading()
            }
        })

        mainViewModel.retrieveListCategory().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                setAdapterCategory(it)
            }
        })

        mainViewModel.retrieveListProductSale().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                setAdapterProductSale(it)
            }
        })

//        mainViewModel.loadData()
        mainViewModel.setListCategory(listCategory)
        mainViewModel.setListProductSale(listProductSale)

        return fragmentView
    }

    // perform on click
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.et_main_search_product -> openSearchProduct()
        }
    }

    // connect data to recycler view via adapter
    private fun setAdapterCategory(list: List<CategoryItem?>?) {

        val adapter = MainCategoryAdapter(context?.applicationContext!!, list)
        rvCategory.adapter = adapter
        rvCategory.layoutManager =
            LinearLayoutManager(context?.applicationContext, RecyclerView.HORIZONTAL, false)
        rvCategory.adapter?.notifyDataSetChanged()

        adapter.setItemClickListener(object :
            MainCategoryAdapter.OnItemClickListener {
            override fun onClick(view: View, item: CategoryItem?) {
//                openArticleNews(item?.name ?: "")
            }
        })

    }

    // connect data to recycler view via adapter
    private fun setAdapterProductSale(list: List<ProductItem?>?) {

        val adapter = MainProductSaleAdapter(context?.applicationContext!!, list)
        rvProductSale.adapter = adapter
        rvProductSale.layoutManager =
            LinearLayoutManager(context?.applicationContext, RecyclerView.VERTICAL, false)
        rvProductSale.adapter?.notifyDataSetChanged()

        adapter.setItemClickListener(object :
            MainProductSaleAdapter.OnItemClickListener {
            override fun onClick(view: View, item: ProductItem?) {
                openDetailProduct(item)
            }
        })

    }

    // open search product activity
    private fun openSearchProduct() {
        val intent = Intent(context?.applicationContext, SearchActivity::class.java)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    // open detail product activity
    private fun openDetailProduct(item: ProductItem?) {
        val intent = Intent(context?.applicationContext, DetailProductActivity::class.java)
        intent.putExtra(KEY_PRODUCT_ID, item?.id)
        intent.putExtra(KEY_PRODUCT_TITLE, item?.title)
        intent.putExtra(KEY_PRODUCT_LOVED, item?.loved)
        intent.putExtra(KEY_PRODUCT_PICTURE_URL, item?.imageUrl)
        intent.putExtra(KEY_PRODUCT_PRICE, item?.price)
        intent.putExtra(KEY_PRODUCT_DESCRIPTION, item?.description)
        Log.e(
            LOG_MAIN_MENU,
            "open detail product = ${item?.id}, ${item?.title}, ${item?.loved}, ${item?.price}, ${item?.imageUrl}, ${item?.price}, ${item?.description}"
        )
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }


    // show load progress
    private fun showLoading() {
//        loadingProgressBar.visibility = View.VISIBLE
        Utilities.disableScreenAction(activity?.window!!)
    }

    // hide progress
    private fun hideLoading() {
//        loadingProgressBar.visibility = View.GONE
        Utilities.enableScreenAction(activity?.window!!)
    }

    // pop up message
    fun showToast(message: String) {
        Toast.makeText(context?.applicationContext, message, Toast.LENGTH_LONG).show()
    }

    // constant value
    companion object {
        const val KEY_PRODUCT_ID: String = "productId"
        const val KEY_PRODUCT_TITLE: String = "productTitle"
        const val KEY_PRODUCT_PICTURE_URL: String = "productURL"
        const val KEY_PRODUCT_DESCRIPTION: String = "productDescription"
        const val KEY_PRODUCT_PRICE: String = "productPrice"
        const val KEY_PRODUCT_LOVED: String = "productLoved"
        const val LOG_MAIN_MENU: String = "mainMenu"
    }
}