package com.example.sehatq.screen.main.ui.profile

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sehatq.model.HistoryProduct
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ProfileViewModel(val context: Context) : ViewModel(), CoroutineScope {

    private var repository = ProfileRepository(context)
    var list: MutableLiveData<List<HistoryProduct?>?> = MutableLiveData()
    var isLoading = MutableLiveData<Boolean>()
    private var job = Job()

    // set co routine
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    // check if app still update data or not
    fun retrieveIsLoading(): MutableLiveData<Boolean> = isLoading

    // retrieve list value
    fun retrieveListData(): MutableLiveData<List<HistoryProduct?>?> = list

    // load from local database
    fun loadContentLocal() {
        launch {
            isLoading.postValue(true)
            val listLocal= repository.loadHistoryPurchase()
            list.postValue(listLocal)
            isLoading.postValue(false)
        }
    }
}